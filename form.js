let listaEstudiantes = [];

const objEstudiantes = {
    nombre: '',
    apellido : '',
    curso : ''
}

let editando = false;

const formulario = document.querySelector('#formulario');
const nombreInput = document.querySelector('#nombre');
const apellidoInput = document.querySelector('#apellido');
const cursoInput = document.querySelector('#curso');
const btnAgregarInput = document.querySelector('#btnAgregar');

formulario.addEventListener('submit', validarFormulario);

function validarFormulario(e) {
    e.preventDefault();

    if(nombreInput.value === '' || apellidoInput.value === '' || cursoInput.value === '') {
        alert('Todos los campos se deben ser llenados');
        return;
    }

    if(editando) {
        editarEstudiante ();
        editando = false;
    } else {
        objEstudiantes.nombre = nombreInput.value;
        objEstudiantes.apellido = apellidoInput.value;
        objEstudiantes.curso = cursoInput.value

        agregarEstudiante();
    }
}

function agregarEstudiante() {

    listaEstudiantes.push({...objEstudiantes});

    mostrarEstudiante();

    formulario.reset();
    limpiarEstudiante();
}

function limpiarEstudiante() {
    objEstudiantes.nombre = '';
    objEstudiantes.apellido = '';
    objEstudiantes.curso = '';
}

function mostrarEstudiante() {
    limpiarHTML();

    const divPacientes = document.querySelector('.div-estudiante');

    listaEstudiantes.forEach(estudiante => {
        const {nombre, apellido, curso} = estudiante;

        const parrafo = document.createElement('p');
        parrafo.textContent = `${nombre} - ${apellido} -${curso} `;

        const editarBoton = document.createElement('button');
        editarBoton.onclick = () => cargarEstudiante(estudiante);
        editarBoton.textContent = 'Editar';
        editarBoton.classList.add('btn', 'btn-editar');
        parrafo.append(editarBoton);

        const eliminarBoton = document.createElement('button');
        eliminarBoton.onclick = () => eliminarEstudiante(curso);
        eliminarBoton.textContent = 'Eliminar';
        eliminarBoton.classList.add('btn', 'btn-eliminar');
        parrafo.append(eliminarBoton);

        const hr = document.createElement('hr');

        divPacientes.appendChild(parrafo);
        divPacientes.appendChild(hr);
    });
}

function cargarEstudiante(estudiante) {
    const { nombre, apellido, curso} = estudiante;

    nombreInput.value = nombre;
    apellidoInput.value = apellido;
    cursoInput.value = curso;


    formulario.querySelector('button[type="submit"]').textContent = 'Actualizar';

    editando = true;
}

function editarEstudiante() {

    objEstudiantes.nombre = nombreInput.value;
    objEstudiantes.apellido = apellidoInput.value;

    listaEstudiantes.map(estudiante => {

        if(estudiante.curso === objEstudiantes.curso) {
            estudiante.nombre = objEstudiantes.nombre;
            estudiante.apellido = objEstudiantes.apellido;
            estudiante.curso = objEstudiantes.curso;

        }

    });

    limpiarHTML();
    mostrarEstudiante();
    formulario.reset();

    formulario.querySelector('button[type="submit"]').textContent = 'Agregar Estudiante';

    editando = false;
}

function eliminarEstudiante(curso) {

    listaEstudiantes = listaEstudiantes.filter(estudiante => estudiante.curso !== curso);

    limpiarHTML();
    mostrarEstudiante();
}

function limpiarHTML() {
    const divPacientes = document.querySelector('.div-estudiante');
    while(divPacientes.firstChild) {
        divPacientes.removeChild(divPacientes.firstChild);
    }
}